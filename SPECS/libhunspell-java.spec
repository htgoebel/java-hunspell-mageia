Summary:	Java bindings for the hunspell spellchacking library
Name:		libhunspell-java
Version:	20100101
Prefix:		hunspell
Release:	%mkrel 1
Group:		Development/Java
License:	LGPL/GPL/MPL
URL:		http://dren.dk/hunspell.html

BuildArch:	noarch

# download from	https://github.com/dren-dk/HunspellJNA/archive/master.zip
Source:	HunspellJNA-master.zip

#Source10:	README.Mageia

BuildRequires:	java-devel
BuildRequires:	ant
BuildRequires:	jna
BuildRequires:	libhunspell-devel

Requires:	java
Requires:	jna
Requires:	libhunspell

Patch10: 10_no-native-libs.patch
Patch20: 20_static-libname.patch

%description
Hunspell is a spell checker and morphological analyzer library and
program designed for languages with rich morphology and complex word
compounding or character encoding.

This package provides the bindings allowing to use libhunspell though
the Java programming language.


%package javadoc
Summary: Java documentation for %{name}
Group: Development/Java
Requires: jpackage-utils

%description javadoc
API documentation for %{name}.

%prep
%setup -q -n HunspellJNA-master
%patch10 -p1
%patch20 -p1
# thow away prebuild stuff comming with HunspellJNA
rm -rf native-src native-lib lib/jna.jar

%build
CLASSPATH=$CLASSPATH:%{_javadir}/jna.jar
export CLASSPATH
# do not build the sample applications, its very inflexible anyways
rm src/dk/dren/hunspell/HunspellMain.java
ant jar docs


%install
rm -fr %{buildroot}
install -d -m0755 %{buildroot}%{_javadir}
install -d -m0755 %{buildroot}%{_javadocdir}/%{name}-%{version}
cp build/jar/%{prefix}.jar %{buildroot}%{_javadir}/%{prefix}-%{version}.jar
%{__ln_s} %{prefix}-%{version}.jar %{buildroot}%{_javadir}/%{prefix}.jar
cp -r build/javadocs/* %{buildroot}%{_javadocdir}/%{name}-%{version}
%{__ln_s} %{name}-%{version} %{buildroot}%{_javadocdir}/%{name}

%clean
rm -rf %{buildroot}

%files
%doc readme.txt COPYING.txt
%{_javadir}/*.jar

%files javadoc
%{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}

%changelog
* Sun Mar 10 2013 Hartmut Goebel <hgoebel@crazy-compilers.com>
- initial version
