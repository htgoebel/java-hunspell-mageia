
temporary location for packaging the Hunspell JNA for Mageia
=============================================================

The software is distributed under General Public License version 2 or
later.


For building the rpm-package in the current directory, use::

  rpmbuild -ba --define='%_topdir '"$PWD" SPECS/libhunspell-java.spec


Testing
---------

CLASSPATH=/usr/share/java/jna.jar:BUILD/HunspellJNA-master/build/jar/hunspell.jar javac SOURCES/TestHunspell.java
CLASSPATH=SOURCES:/usr/share/java/jna.jar:BUILD/HunspellJNA-master/build/jar/hunspell.jar java TestHunspell


Non-Standard dependencies
----------------------------



Todo packaging
-----------------
